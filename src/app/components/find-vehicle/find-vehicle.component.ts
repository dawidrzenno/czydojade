import { Subscription} from 'rxjs';
import { FindVehicleService, Coordinates, T } from './find-vehicle.service';
import { Component, OnInit, OnDestroy} from '@angular/core';

@Component({
  selector: 'app-find-vehicle',
  templateUrl: './find-vehicle.component.html',
  styleUrls: ['./find-vehicle.component.scss']
})
export class FindVehicleComponent implements OnInit, OnDestroy {

  lines: string[];
  selectedLines: string[];
  coordinates: Coordinates;
  subscriptions = new Subscription();
  httpErrors: string[];
  t: T;

  constructor(
    private findVehicleService: FindVehicleService
  ) {
    this.httpErrors = [];
    this.lines = [];
    this.selectedLines = [];
  }

  getLines() {
    const linesSubscription = this.findVehicleService.getLines().subscribe(lines => {
      this.lines = lines;
      this.httpErrors.push('Success while getting lines.');
    }, error => {
      this.httpErrors.push('Error while getting lines. ' + error);
    });
    this.subscriptions.add(linesSubscription);
  }

  changeLineList(selectedLine: string) {
    const linePatch = [];
    let removeSelectedLine = false;

    this.selectedLines.forEach(existingLine => {
      if (selectedLine === existingLine) {
        removeSelectedLine = true;
      } else {
        linePatch.push(existingLine);
      }
    });

    if (removeSelectedLine === false) {
      linePatch.push(selectedLine);
    }

    this.selectedLines = linePatch;
    console.log(this.selectedLines);

    this.getCoordinates(this.selectedLines);
  }

  getCoordinates(selectedLines: string[]) {
    const selectedLinesBusList: T = {};
    selectedLinesBusList["busList[bus][]"] = selectedLines;
    const coordinatesSubscription = this.findVehicleService.getCurrentVehicleCoordinates(selectedLinesBusList)
    .subscribe(response => {
      this.coordinates = response;
      this.httpErrors.push('Success while getting coordinates.');
    }, error => {
      this.httpErrors.push('Error while getting coordinates. ' + error);
    });
    this.subscriptions.add(coordinatesSubscription);
  }

  ngOnDestroy() {
    this.subscriptions.unsubscribe();
  }

  ngOnInit() {
    this.getLines();
  }
}
