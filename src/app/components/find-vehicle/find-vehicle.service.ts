import { Injectable } from '@angular/core';
import { HttpClient, HttpErrorResponse, HttpHeaders } from '@angular/common/http';
import { throwError, of } from 'rxjs';
import { catchError, retry} from 'rxjs/operators';

export interface Coordinates {
  name: string;
  type: string;
  y: string;
  x: string;
  k: string;
}

export interface T {
  'busList[bus][]'?: any[];
  'busList[tram][]'?: any[];
  'busList[train][]'?: any[];
}

@Injectable({
  providedIn: 'root'
})

export class FindVehicleService {

  currentVehicleCoordinatesUrl = 'http://mpk.wroc.pl/position.php';
  httpOptions = {
    headers: new HttpHeaders({
      'Content-Type': 'application/x-www-form-urlencoded'
    })
  };

  constructor(private http: HttpClient) { }

  getLines() {
    return of(['A', 'C', 'D', 'K', 'N']);
  }

  getCurrentVehicleCoordinates(t: T) {

    return this.http.post<Coordinates>(this.currentVehicleCoordinatesUrl, t, this.httpOptions).pipe(
      catchError(httpError => this.handleHttpError(httpError)) // handle the error
    );
  }

  handleHttpError(httpError: HttpErrorResponse) {
    console.log(this.httpOptions);
    if (httpError.error instanceof ErrorEvent) {
      // A client-side or network error occurred. Handle it accordingly.
      console.error('An error occurred:', httpError.error.message);
    } else {
      // The backend returned an unsuccessful response code.
      // The response body may contain clues as to what went wrong,
      console.error(
        `Backend returned code ${httpError.status}, ` +
        `body was: ${httpError.error}`);
    }
    // return an observable with a user-facing error message
    return throwError('Something bad happened; please try again later.');
  }
}
