import { TestBed } from '@angular/core/testing';

import { FindVehicleService } from './find-vehicle.service';

describe('FindVehicleService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: FindVehicleService = TestBed.get(FindVehicleService);
    expect(service).toBeTruthy();
  });
});
