function ajaxData() {
  var x = {
    bus: [
    ],
    tram: [
    ],
    train: [
    ]
  };
  for (type in busList) {
    for (i in busList[type]) {
      x[type][x[type].length] = jQuery.trim(i);
    }
  }
  return x;
}
function initGmap() {
  myLatlng = new google.maps.LatLng(lat, lng);
  mapOptions = {
    zoom: zoom,
    center: myLatlng,
    mapTypeId: google.maps.MapTypeId.ROADMAP,
    scrollwheel: false,
    disableDoubleClickZoom: true,
    styles: [
      {
        'featureType': 'landscape',
        'stylers': [
          {
            'saturation': - 100
          },
          {
            'lightness': 20
          },
          {
            'visibility': 'on'
          }
        ]
      },
      {
        'featureType': 'poi',
        'stylers': [
          {
            'saturation': - 100
          },
          {
            'lightness': 20
          },
          {
            'visibility': 'simplified'
          }
        ]
      },
      {
        'featureType': 'road.highway',
        'stylers': [
          {
            'saturation': - 100
          },
          {
            'visibility': 'simplified'
          }
        ]
      },
      {
        'featureType': 'road.arterial',
        'stylers': [
          {
            'saturation': - 100
          },
          {
            'visibility': 'on'
          }
        ]
      },
      {
        'featureType': 'road.local',
        'stylers': [
          {
            'saturation': - 100
          },
          {
            'lightness': 10
          },
          {
            'visibility': 'on'
          }
        ]
      },
      {
        'featureType': 'transit',
        'stylers': [
          {
            'saturation': - 100
          },
          {
            'visibility': 'simplified'
          }
        ]
      },
      {
        'featureType': 'administrative.province',
        'stylers': [
          {
            'visibility': 'off'
          }
        ]
      },
      {
        'featureType': 'water',
        'elementType': 'labels',
        'stylers': [
          {
            'visibility': 'on'
          },
          {
            'saturation': - 100
          }
        ]
      },
      {
        'featureType': 'water',
        'elementType': 'geometry',
        'stylers': [
          {
            'hue': '#ffff00'
          },
          {
            'saturation': - 100
          }
        ]
      }
    ]
  };
  mapElement = document.getElementById('bus-map');
  map = new google.maps.Map(mapElement, mapOptions);
  //                marker = new google.maps.Marker({
  //                    position: myLatlng,
  //                    map: map
  //                });
  infoWindow = new google.maps.InfoWindow();
  google.maps.event.addDomListener(window, 'resize', function () {
    map.setCenter(mapOptions.center);
  });
}
function mapLoad3() {
  initGmap();
  pobieraj();
}
function dodajAktualizujMarker3(data) {
  data.name0 = data.name;
  data.name = data.name.toLowerCase();
  if (busList[data.type][data.name]) {
    if (!busList[data.type][data.name][data.k]) {
      busList[data.type][data.name][data.k] = {
      };
    }
    if (busList[data.type][data.name][data.k]['marker']) {
      var latlng = new google.maps.LatLng(data.x, data.y);
      busList[data.type][data.name][data.k]['marker'].setPosition(latlng);
    } else {
      var img_url = baseBusNewMarkerUrl + '/' + data.type + '-' + data.name0 + '.png?1';
      var pinImage = new google.maps.MarkerImage(img_url, null, new google.maps.Point(0, 0), new google.maps.Point(16, 20));
      var marker = new google.maps.Marker({
        position: new google.maps.LatLng(data.x, data.y),
        map: map,
        icon: pinImage,
        shadow: null
      });
      busList[data.type][data.name][data.k]['marker'] = marker;
      busList[data.type][data.name][data.k]['name'] = data.name0;
      busList[data.type][data.name][data.k]['type'] = data.type;
      marker.setMap(map);
    }
  }
}
function countProperties(obj) {
  var count = 0;
  for (var prop in obj) {
    if (obj.hasOwnProperty(prop)) {
      count++;
    }
  }
  return count;
}
function usunMartwePojazdy(data) {
  var kk = {
  };
  if ((typeof data == 'object') && (data.length > 0)) {
    for (var i in data) {
      kk[data[i].k] = true;
    }
  }
  for (var type in busList) {
    for (var name in busList[type]) {
      for (var k in busList[type][name]) {
        if ((!kk[k]) || (kk[k].name != name) || (kk[k].type != type)) {
          try {
            busList[type][name][k]['marker'].setMap(null);
            delete busList[type][name][k];
          } catch (err) {
          }
        }
      }
    }
  }
}
function pobieraj(re) {
  if (!is_active_ajax) {
    if ((countProperties(busList.bus) > 0) || (countProperties(busList.tram) > 0) || (countProperties(busList.train) > 0)) {
      is_active_ajax = true;
      jQuery.ajax({
        type: 'POST',
        url: baseBusPosUrl,
        dataType: 'json',
        crossDomain: true,
        data: {
          busList: ajaxData()
        },
      }).done(function (data) {
        usunMartwePojazdy(data);
        for (var i in data) {
          dodajAktualizujMarker3(data[i]);
        }
        is_active_ajax = false;
      }).fail(function () {
        is_active_ajax = false;
      });
    }
  }
  if (!re) {
    setTimeout(pobieraj, 15000);
  }
}
function lineClick(obj, type, cssClass) {
  var a = jQuery(obj).text().toLowerCase();
  jQuery(obj).toggleClass(cssClass);
  if (busList[type][a]) {
    for (var k in busList[type][a]) {
      if (busList[type][a][k]['marker']) {
        busList[type][a][k]['marker'].setMap(null);
      }
    }
    delete busList[type][a];
  } else {
    busList[type][a] = {
    };
  }
  pobieraj(true);
}
jQuery(document).ready(function () {
  jQuery('.bus-gps-lines.bus ul li').click(function () {
    lineClick(this, 'bus', 'active');
  });
  jQuery('.bus-gps-lines.tram ul li').click(function () {
    lineClick(this, 'tram', 'active');
  });
  jQuery('.bus-gps-lines.train ul li').click(function () {
    lineClick(this, 'train', 'active');
  });
  mapLoad3();
});
